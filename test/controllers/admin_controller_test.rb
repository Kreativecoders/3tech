require 'test_helper'

class AdminControllerTest < ActionController::TestCase
  test "should get about" do
    get :about
    assert_response :success
  end

  test "should get blog_inner" do
    get :blog_inner
    assert_response :success
  end

  test "should get blog_sidebar_left" do
    get :blog_sidebar_left
    assert_response :success
  end

  test "should get contact" do
    get :contact
    assert_response :success
  end

  test "should get index_2" do
    get :index_2
    assert_response :success
  end

  test "should get portfolio_default" do
    get :portfolio_default
    assert_response :success
  end

  test "should get services" do
    get :services
    assert_response :success
  end

end
