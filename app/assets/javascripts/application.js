// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//

//= require "jquery.js"
//= require "foundation.min.js"
//= require "modernizr.foundation.js"  
//= require "jquery.themepunch.plugins.min.js"
//= require "jquery.themepunch.revolution.min.js"
//= require "jquery.tipsy.js"
//= require "jquery.carouFredSel-6.0.3-packed.js"
//= require "jquery.touchSwipe.min.js"
//= require "jquery.flexslider.js"
//= require "jquery.countdown.js"
//= require "jquery.easing.1.3.js"
//= require "jquery.isotope.min.js"
//= require "jquery.titanlighbox.js"
//= require "jquery.mobile.customized.min.js"
//=	require "app.js"
//= require "app-head.js"
//= require "prettify.js"
//= require "camera.min.js"
