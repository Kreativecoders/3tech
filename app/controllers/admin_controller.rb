class AdminController < ApplicationController
  def about
  end

  def blog_inner
  end

  def blog_sidebar_left
  end

  def contact
  @qucikcontact=Contact.new  
  end

  def index_2
  end

  def portfolio_default
  end

  def services
  end
  def single_project
  end
  def new
   @qucikcontact=Contact.new
 end
  def create
    @qucikcontact=Contact.new(params.require(:contact).permit(:name, :email, :subject, :message))
    @qucikcontact.save
    ContactUsmailer.contactus(@qucikcontact).deliver
    redirect_to root_path
  end
end
