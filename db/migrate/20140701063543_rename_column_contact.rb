class RenameColumnContact < ActiveRecord::Migration
  def change
  	 rename_column :contacts, :subject, :message
  end
end
